import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
var green = '#39D1B4';

class Thing1 extends React.Component{
  render() {
    return (
      <div className = "div-1" >
      
      </div>
    );
  }
};

class Thing2 extends React.Component{
  render() {
    return (
      <div className="div-2">
	 
      </div>
    );
  }
};

class Thing3 extends React.Component{
  render() {
    return (
      <div className="div-3">
       
      </div>
    );
  }
};



class Toggle extends React.Component{
	constructor(props){
		super(props);
		this.state = {
      color: green,
    };	
  }
	
	randomColor = () => {
	    var letters = '0123456789ABCDEF';
	    var colory = '#';
	    for (var i = 0; i < 6; i++ ) {
		  colory += letters[Math.floor(Math.random() * 16)];
	    };
	    this.setState({
	    	color: colory,
	    });
					 
	}
	
	render(){
		return (
			<div style={{background: this.state.color}}>
				<button onClick={this.randomColor}>Make change</button>
			<div>
				<p> <Thing1/> </p>
				<p> <Thing2/> </p>
				<p> <Thing3/> </p>
			</div>	
			</div>
		);
	}
}

ReactDOM.render(<Toggle />, document.getElementById('app'));
